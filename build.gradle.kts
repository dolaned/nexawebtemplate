import org.gradle.kotlin.dsl.register
import java.io.File
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

val kotlinVersion: String by project
val logbackVersion: String by project
val kotlinxHtmlJvmVersion: String by project
val zxingVersion: String by project
val ktorVersion: String = "3.1.0"  // https://github.com/ktorio/ktor
val coroutinesVersion: String = "1.10.1" // https://github.com/Kotlin/kotlinx.coroutines/releases

tasks.shadowJar {
    isZip64 = true
}

tasks.jar {
    exclude("frontends/vuenuxt")
}

open class BuildVueNuxtTask : DefaultTask() {
    @TaskAction
    fun action()
    {
        project.exec {
            commandLine("bash", "-c", "(cd src/main/resources/frontends/vuenuxt; npm run build; npm run generate)")
        }
    }
}

open class SetupVueNuxtTask : DefaultTask() {
    @TaskAction
    fun action()
    {
        // If you get an error here, "bash: line 1: npm: command not found", the problem is that npm is not in your path
        // Recall that bash -c does NOT source .bashrc
        // place your path in .profile (GUI) and .bash_profile (SSH/TTY) to handle both login types, and log out (or restart)
        project.exec {
            commandLine("bash", "-c", "(cd src/main/resources/frontends/vuenuxt; npm install)")
        }
    }
}

val VUE_NUXT_FE_DIR = "src/main/resources/frontends/vuenuxt"

tasks.register<SetupVueNuxtTask>("setupvuenuxt") {
    group = "setup"
    description = "Install npm and dependencies into the Vue/Nuxt front end"
    inputs.files(VUE_NUXT_FE_DIR + "/package.json", VUE_NUXT_FE_DIR + "/package-lock.json")

    outputs.dir(VUE_NUXT_FE_DIR + "/node_modules")
}

tasks.register<BuildVueNuxtTask>("vuenuxt") {
    group = "build"
    description = "Build the Vue/Nuxt front end"
    inputs.dir(VUE_NUXT_FE_DIR + "/components")
    inputs.dir(VUE_NUXT_FE_DIR + "/composables")
    inputs.dir(VUE_NUXT_FE_DIR + "/layouts")
    inputs.dir(VUE_NUXT_FE_DIR + "/static")
    inputs.dir(VUE_NUXT_FE_DIR + "/server")
    inputs.dir(VUE_NUXT_FE_DIR + "/node_modules")

    outputs.dir(VUE_NUXT_FE_DIR + "/.output")
    dependsOn("setupvuenuxt")
}
tasks.named<ProcessResources>("processResources") {
    // Only include Vue/Nuxt inputs when not running 'test'
    if (!gradle.startParameter.taskNames.contains("test")) {
        inputs.dir("$VUE_NUXT_FE_DIR/node_modules").optional()
        inputs.dir("$VUE_NUXT_FE_DIR/.output").optional()
        dependsOn("setupvuenuxt")
        dependsOn("vuenuxt")
    }
}

tasks.named("build")
{
    dependsOn("vuenuxt")
}

plugins {
    // Apply the application plugin for building a JVM application
    application
    // Apply the Kotlin JVM plugin for Kotlin support
    kotlin("jvm") version "2.1.10"
    // Apply the Ktor plugin for Ktor server support
    id("io.ktor.plugin") version "3.1.0"
    id("org.jetbrains.dokka") version "2.0.0"
    // Apply the Kotlin Serialization plugin for JSON serialization
    kotlin("plugin.serialization") version "2.1.10"
    id("idea")
}

group = "org.nexa"
version = "0.0.4"

application {
    // Specify the main class for the application
    mainClass.set("org.nexa.ApplicationKt")

    // Set the default JVM arguments for the application
    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    // Define repositories for dependencies
    gradlePluginPortal()
    google()
    mavenCentral()
    // Nexa repositories for specific libraries
    maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpthreads
    maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") }  // Nexa RPC into nexad
    maven { url = uri("https://gitlab.com/api/v4/projects/46299034/packages/maven") }  // Nexa Script Machine
    maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") }  // LibNexaKotlin
    maven { url = uri("https://gitlab.com/api/v4/projects/15615113/packages/maven") }  // Wally Enterprise Wallet
    mavenLocal()
}

dependencies {
    // Define project dependencies
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.6.0")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-core-jvm:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-mustache-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-websockets-jvm:$ktorVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:$kotlinxHtmlJvmVersion")
    implementation("io.ktor:ktor-server-html-builder-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-metrics-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-call-logging:$ktorVersion")
    implementation("io.ktor:ktor-server-caching-headers-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-webjars-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-host-common-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-sessions-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-netty-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-cors-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-call-logging:$ktorVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    // testImplementation("io.ktor:ktor-server-tests-jvm:2.3.13")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")

    implementation("org.webjars:jquery:3.7.1") // https://github.com/webjars/jquery/tags

    // Dependencies for BigIntegers/BigDecimals
    implementation("com.ionspin.kotlin:bignum:0.3.9")
    implementation("com.ionspin.kotlin:bignum-serialization-kotlinx:0.3.9")

    // QR code creation dependency
    implementation("com.google.zxing:core:$zxingVersion")

    // Nexa specific dependencies
    implementation("org.nexa:mpthreads:0.4.0")  // https://gitlab.com/nexa/mpthreads/-/packages
    implementation("org.nexa:libnexakotlin:0.4.11") // https://gitlab.com/nexa/libnexakotlin/-/packages
    implementation("org.nexa:scriptmachine:1.2.2")
    implementation("org.wallywallet:wew:1.3.3") // https://gitlab.com/wallywallet/enterprise/-/packages
    implementation("io.ktor:ktor-server-content-negotiation")
    implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.5.0")

    testImplementation("io.ktor:ktor-client-core:$ktorVersion")
    testImplementation("io.ktor:ktor-server-test-host:$ktorVersion") // For testApplication
    testImplementation("io.ktor:ktor-server-core:$ktorVersion") // Required for testing
}

val BASEDIR = "org.nexa.nexawebtemplate-$version"
val BINDIR = "$BASEDIR/bin"

// Task to include additional files in the distribution zip
tasks.getByName<Zip>("distZip") {
    with(copySpec {
        //from("libnexa.so").into(BINDIR)
        from("src/main/resources/frontends/vuenuxt/.output").into(BINDIR +"/vuenuxt")
    })
}

idea {
    module {
        excludeDirs.add(file("nexaWebTemplate/build"))
        excludeDirs.add(file("build"))
        excludeDirs.add(file("src/main/resources/frontends/vuenuxt/.output"))
    }
}
