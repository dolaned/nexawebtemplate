package org.nexa
import io.ktor.http.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import kotlin.test.*
import io.ktor.server.testing.*
import org.nexa.plugins.*
import org.wallywallet.wew.WallyEnterpriseWallet

class ApplicationTest
{
    @Test
    fun testRoot()
    {
        CONFIG_FILE = "regtestapp.cfg"
        testApplication {
            loadConfigAndInit()
            loadWallet()
            application {
                configureSerialization()
                configureTemplating()
                configureSockets()
                configureMonitoring()
                configureHTTP()
                configureSecurity()
                configureRouting()
            }
            client.get("/kdsl").apply {
                assertEquals(HttpStatusCode.OK, status)
                val homePage = bodyAsText()
                check("Example" in homePage)
            }

            // Testing Nuxt/Vue is going to be hard because the whole page is served dynamically
            // No there's basically nothing in a classic "get".  We will need to actually execute the page.
            client.get("/").apply {
                assertEquals(HttpStatusCode.OK, status)
                val homePage = bodyAsText()
                check("_nuxt" in homePage)
            }
            // This test runs so quickly the wallet does not have a chance to save itself
            for (w in WallyEnterpriseWallet.accounts.values)
            {
                w.save(true)
            }
        }
    }
}
