package org.nexa.plugins

import io.ktor.server.routing.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.response.*
import io.ktor.server.webjars.*
import org.nexa.SERVER_FQDN
import org.nexa.appConfig
import org.nexa.SharedResources.activeWallet
import org.nexa.api.controllers.getFaucetData
import org.nexa.api.controllers.getFaucetQrData
import org.nexa.api.controllers.getIdentityQRData
import org.nexa.api.controllers.*
import org.nexa.api.controllers.getReverseQRData
import org.nexa.api.controllers.getSignMessageData
import org.nexa.api.controllers.getTdppQRData
import org.nexa.api.controllers.getWalletDetailsData
import org.nexa.api.controllers.getWelcomeData
import org.nexa.frontends.kotlindsl.homePage
import org.nexa.libnexakotlin.CommonWallet
import org.nexa.sessionNewPage
import java.io.File

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()

var VUENUXT_FRONTEND_PREFIX = "vuenuxt"  // Overridden in the app.cfg file
var REACT_FRONTEND_PREFIX = "vuenuxt"  // Overridden in the app.cfg file

fun VueNuxtOutput(projectName: String, suffix: String): File = File("$VUENUXT_FRONTEND_PREFIX/public$suffix")
fun ReactStatic(projectName: String, suffix: String): File = File("$REACT_FRONTEND_PREFIX/out$suffix")

suspend fun ApplicationCall.excWrapper(where: String, body: suspend () -> Unit)
{
    try
    {
        body()
    }
    catch (e: java.io.IOException)
    {
        handleException(where, e)
    }
}

suspend fun ApplicationCall.handleException(info: String, e: Exception)
{
    respondText("$info\nActual Error:\n${e.stackTraceToString()}")
}

fun Application.configureRouting()
{
    install(Webjars) {
        path = "/webjars"
    }
    install(StatusPages) {
        exception<AuthenticationException> { call, _ ->
            call.respond(HttpStatusCode.Unauthorized)
        }
        exception<AuthorizationException> { call, _ ->
            call.respond(HttpStatusCode.Forbidden)
        }
    }
    install(CORS) {
        anyHost()
    }

    routing {

        /* FRONTEND-ROUTES */
        for (clientName in appConfig.activeClients)
        {
            val client = appConfig.client[clientName]
            if (client == null)
            {
                println("CONFIGURATION ERROR: Unknown active client $clientName.  Defined clients are ${appConfig.client.keys}.")
                throw IllegalArgumentException("CONFIGURATION ERROR: Unknown active client $clientName.  Defined clients are ${appConfig.client.keys}.")
                // continue
            }

            // Extract the project name from the client location in the configuration
            val location = client.location
            val projectName = location.substringAfterLast("/")

            // Extract the framework name from the configuration
            val framework = client.framework
            val prefix = client.routeprefix

            // Handle different frontend frameworks
            when (framework.lowercase())
            {
                "react" ->
                {
                    // Serve static files for a React project
                    staticFiles("/_next", ReactStatic(projectName,"/_next"))
                    staticFiles("/", ReactStatic(projectName,""))
                    staticFiles("/_next/static", ReactStatic(projectName,"/_next/static"))

                    // Serve the React index.html file at the root URL
                    get("/") {
                        call.respondFile(File("src/main/resources/frontends/$projectName/out/index.html"))
                    }
                }

                "vuenuxt" ->
                {
                    // Serve static files for a Vue project
                    staticFiles("/_nuxt", File("$location/dist/_nuxt"))
                    staticFiles(prefix, File("$location/dist"))

                    // Serve the Vue index.html file at the root URL
                    get(prefix) {
                        call.excWrapper("Vue/Nuxt front end is not built!") {
                            call.respondFile(File("$location/dist/index.html"))
                         }
                    }
                }

                "nuxt-faucet" -> {
                    // Serve static files for a Nuxt-Faucet project
                    staticFiles("$prefix/_nuxt", VueNuxtOutput(projectName,"/_nuxt"))
                    staticFiles(prefix, VueNuxtOutput(projectName,""))

                    // Serve the Nuxt-Faucet index.html file at the root URL defined by routeprefix
                    get(prefix) {
                        call.excWrapper("Vue/Nuxt front end is not built!") {
                            call.respondFile(VueNuxtOutput(projectName, "/index.html"))
                        }
                    }
                }

                "kotlindsl" ->
                {
                    // This is just an example, you can call kotlin functions, and they will be injected into Routing.kt
                    get(prefix) {
                        homePage(call)
                    }
                    get("/admin/reassess")
                    {
                        val wal = activeWallet as CommonWallet
                        //wal.reassessUnconfirmedTx()
                        wal.cleanUnconfirmed()
                        wal.rediscover()
                        homePage(call)
                    }
                    //staticFiles("/static", File("$location/static"))
                    staticResources("/static", "kotlindsl")
                }

                "custom" ->
                {
                    // Add whatever you want here
                }

                else -> throw IllegalArgumentException("Unsupported framework: $framework")
            }
        }

        /* BACKEND-ROUTES */
        get("/api/welcome")
        {
            getWelcomeData(call)
        }
        get("/api/wallet")
        {
            getWalletDetailsData(call)
        }
        get("/api/wallet/disconnect")
        {
            disconnectWallet(call)
        }
        get("/api/faucet")
        {
            getFaucetData(call)
        }
        get("/api/faucet/qr")
        {
            getFaucetQrData(call)
        }
        post("/api/faucet/address")
        {
            createAddressFaucetTx(call)
        }
        get("/api/signMessage")
        {
            getSignMessageData(call)
        }
        get("/api/identity/qr")
        {
            getIdentityQRData(call)
        }
        get("/api/tdpp/qr")
        {
            getTdppQRData(call)
        }
        get("/api/reverseQR/qr")
        {
            getReverseQRData(call)
        }
        get("/api/config") {
            val configFile = File("app.cfg")
            if (configFile.exists()) {
                val configJson = configFile.readText()
                call.respondText(configJson, ContentType.Application.Json)
            } else {
                call.respond(HttpStatusCode.NotFound, "Config file not found")
            }
        }
        get("/session")
        {
            val (id, session) = sessionNewPage(call)
            call.respond(mapOf("sessionId" to session.sessionId?.id, "SERVER_FQDN" to SERVER_FQDN))
        }
        // It wouldn't normally be a great idea to just forward anything from the client to the wallet
        // in case something is put into site javascript or maybe some kind of browser plugin
        get("/_walletRequest")
        {
            handleWalletRequest(call)
        }
        post("/_share")
        {
            handleShare(call)
        }
        // Long Polling between phone and server
        get("/_lp")
        {
            handleLongPolling(call)
        }
        // TDPP tx request coming back from the wallet
        get("/tx")
        {
            processTransaction(call)
        }
        get("/_changeSignMessage")
        {
            changeSignMessage(call)
        }
        post("/_identity")
        {
            postIdentity(call)
        }
        get("/_identity")
        {
            getIdentity(call)
        }
        get("/webjars")
        {
            call.respondText("<script src='/webjars/jquery/jquery.js'></script>", ContentType.Text.Html)
        }
    }
}
