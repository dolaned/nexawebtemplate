package org.nexa.plugins

import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import io.ktor.websocket.*
import java.time.Duration
import io.ktor.server.application.*
import kotlinx.coroutines.launch
import java.util.logging.Logger
import org.nexa.sessionFromCookie
import java.time.Instant
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.time.Duration.Companion.seconds

private val LogIt = Logger.getLogger("org.nexa.plugins.Sockets")

object WebSocketManager {
    val activeSessions = CopyOnWriteArrayList<WebSocketSession>()
}

fun Application.configureSockets() {
    install(WebSockets) {
        pingPeriod = 15.seconds  // Duration.ofSeconds(15)
        timeout = 15.seconds // Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }

    routing {
        webSocket("/ws") {
            val cookie = call.request.queryParameters["cookie"]
            if (cookie == null) {
                close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "No cookie parameter"))
            } else {
                val session = sessionFromCookie(cookie)
                if (session == null) {
                    close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "Unknown session"))
                } else {
                    // Check if there's an existing WebSocket session and reuse it
                    val existingSession = WebSocketManager.activeSessions.find { it === session.comm }
                    if (existingSession != null) {
                        // Reuse the existing session
                        session.comm = this
                    } else {
                        // Establish a new session
                        session.activeWalletConnection = true
                        session.comm = this
                        session.lastClientConnection = Instant.now()
                        WebSocketManager.activeSessions.add(this)
                    }

                    try {
                        for (frame in incoming) {
                            when (frame) {
                                is Frame.Text -> {
                                    val text = frame.readText()
                                    session.lastClientConnection = Instant.now()
                                    if (text == "ping") {
                                        send("pong")
                                    }
                                }
                                is Frame.Close -> {
                                    session.activeWalletConnection = false
                                }
                                else -> {
                                    // Handle other frame types if needed
                                }
                            }
                        }
                    } finally {
                        session.activeWalletConnection = false
                        session.comm = null
                        WebSocketManager.activeSessions.remove(this)
                    }
                }
            }
        }
    }
}
