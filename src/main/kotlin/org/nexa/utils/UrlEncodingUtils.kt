// src/main/kotlin/org/nexa/utils/UrlEncodingUtils.kt
package org.nexa.utils

import org.nexa.libnexakotlin.PayAddress
import java.net.URLEncoder

fun String.urlEncode(): String {
    return URLEncoder.encode(this, "utf-8")
}

fun PayAddress.urlEncode(): String {
    return toString().urlEncode()
}