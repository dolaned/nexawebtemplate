// src/main/kotlin/org/nexa/utils/MiscUtils.kt
package org.nexa.org.nexa.utils

import org.nexa.api.controllers.MIN_REWARD
import org.nexa.libnexakotlin.OP
import org.nexa.libnexakotlin.PayAddress
import org.nexa.libnexakotlin.iTransaction
import org.nexa.libnexakotlin.txOutputFor
import kotlin.random.Random

fun verifyChallengeTransaction(challenge: ByteArray, challengerId: ByteArray, tx: iTransaction, addr: PayAddress): Boolean
{
    val cs = tx.chainSelector
    // Verify that this is a challenge transaction
    if (tx.version < 0x80) return false
    // Verify that the output is correct
    if (tx.outputs.size != 1) return false
    val dout = tx.outputs[1]
    if (dout.amount != 0L) return false  // data outputs must pay 0

    val s = dout.script.parsed()
    if (s[0] != OP.RETURN.v) return false
    if (!(challengerId contentEquals s[1])) return false
    val chTx = s[2]
    if (chTx.size % 2 != 0) return false // has to be even because should have random bytes interspersed
    val filteredChal = ByteArray(chTx.size/2)
    for (i in 0 until filteredChal.size)
    {
        filteredChal[i] = chTx[i*2 + 1]
    }
    if (!(filteredChal contentEquals challenge)) return false

    val prevout = txOutputFor(cs, 0, addr.lockingScript())

    // TODO need to add cashlib primitive: val result = ScriptMachine.validateScripts(tx, arrayOf(prevout))
    return false
}

fun getRandomSatoshis(maxReward: Long, minReward:Long = MIN_REWARD): Long
{
    return Random.nextLong(minOf(minReward, maxReward), maxReward + 1)  // +1 to include MAX_REWARD
}
