package org.nexa

import org.nexa.libnexakotlin.Wallet
import org.wallywallet.wew.WallyEnterpriseWallet

// Singleton object to hold shared resources
object SharedResources {
    // Default wallet name
    var WALLET_NAME: String = "appWallet"

    // Property to get the active wallet instance
    val activeWallet: Wallet
        get() {
            // Retrieve the wallet instance using the WALLET_NAME
            val wal: Wallet? = WallyEnterpriseWallet.accounts[WALLET_NAME]

            // If the wallet instance is null, throw an error
            if (wal == null) {
                throw (Error("Wallet is inaccessible"))
            }

            // Return the wallet instance
            return wal
        }
}
