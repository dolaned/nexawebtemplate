@file:OptIn(ExperimentalUnsignedTypes::class)

package org.nexa

import org.nexa.libnexakotlin.*
import org.wallywallet.wew.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.nexa.plugins.*
import java.io.File
import java.io.FileNotFoundException
import java.net.URLEncoder
import kotlin.concurrent.thread
import org.nexa.SharedResources.WALLET_NAME
import org.nexa.SharedResources.activeWallet
import org.nexa.api.controllers.initializeWalletListener
import org.nexa.api.models.AppConfig
import org.nexa.api.models.FaucetUsage
import java.util.concurrent.ConcurrentHashMap

private val LogIt = GetLog("org.nexa.app")

var SERVER_FQDN = "192.168.2.106:7997" // Get from config file, include port if not using 80
var APP_TITLE = "Nexa Example"
const val APP_DESCRIPTION = "Web app example and template that shows wallet integration via QR codes"
const val SITE_FLAG_IMG = "/static/logo.svg"

var WALLET_NAME:String = "appWallet"
var RECV_ADDR = ""
var CONFIG_FILE = "app.cfg"
val TDPP_TOPIC = "example"

var NEXA_FULL_NODE_ADDR:String? = "127.0.0.1" // null  // Set in the config file: if the app uses a trusted full node.

var SERVER_IDENTITY_ADDRESS:PayAddress? = null  // You need this server to contain a wallet that can sign with this address

var REFRESH_VERSION = 0L

var FUND_ADDRESS_AMOUNT = 0L

var FUND_ADDRESS_COOLDOWN = 300

lateinit var appConfig:AppConfig

fun String.urlEncode():String
{
    return URLEncoder.encode(this, "utf-8")
}

fun PayAddress.urlEncode():String
{
    return toString().urlEncode()
}

val faucetLimits = ConcurrentHashMap<String, FaucetUsage>()  // IP-based tracking
val addressLimits = ConcurrentHashMap<String, String>()  // Crypto address tracking

fun main()
{
    loadConfigAndInit()
    loadWallet()
    // By not specifying a host parameter, we are accepting any incoming. This allows both direct requests and requests proxied through apache2
    val emb = embeddedServer(Netty, port = 7997) {
        configureSerialization()
        configureTemplating()
        configureSockets()
        configureMonitoring()
        configureHTTP()
        configureSecurity()
        configureRouting()
    }

    println(RECV_ADDR)
    println(SERVER_IDENTITY_ADDRESS)
    thread { walletSessionMonitor() }
    emb.start(wait = true)
}

fun loadConfigAndInit()
{
    REFRESH_VERSION = System.currentTimeMillis()  // Force many files (e.g. css) to refresh every run

    val userDir = System.getProperty("user.dir")
    System.out.println("Current directory: " + userDir)


    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }
    try  // Load config file if it exists and parse parameters
    {
        val cfg = File(CONFIG_FILE).readText(Charsets.UTF_8)
        val parsed = json.decodeFromString(AppConfig.serializer(), cfg)
        appConfig = parsed

        WALLET_NAME = parsed.server.walletFile
        parsed.server.blockchain?.let {
            DEFAULT_CHAIN = uriToChain[it]!!
        }
        System.out.println("Blockchain: " + DEFAULT_CHAIN)

        // Set various globals from the config file structure
        SERVER_FQDN = parsed.server.domainName
        if (parsed.server.adminToken != null) ADMIN_TOKEN = GroupId(parsed.server.adminToken)
        if (parsed.server.adminAddress != null) ADMIN_IDENTITY_ADDRESS = parsed.server.adminAddress
        NEXA_FULL_NODE_ADDR = parsed.server.trustedFullNode
        FUND_ADDRESS_AMOUNT = parsed.server.fundAddress
        FUND_ADDRESS_COOLDOWN = parsed.server.fundAddressCooldown.toInt()
        
        val vuenuxt = parsed.client["vuenuxt"]
        if (vuenuxt != null)
        {
            VUENUXT_FRONTEND_PREFIX = vuenuxt.location ?: ""
        }
    }
    catch(e:FileNotFoundException) 
    {
        println("NO CONFIG FILE")
    }


}
fun loadWallet()
{
    // If you do not init a wallet or blockchain, you do not need the exposed DB as a dependency
    initializeLibNexa()
    thread {
        WallyEnterpriseWallet.run(WallyEnterpriseWallet.CliType.Fifo)
    }
    val wal = try {
        openOrNewWallet(WALLET_NAME, DEFAULT_CHAIN)
    }
    catch(e: Exception) {
        logThreadException(e)
        return
    }
    WallyEnterpriseWallet.accounts[WALLET_NAME] = wal

    val bc = wal.blockchain
    bc.req.MAX_RECENT_BLOCK_CACHE = 500
    bc.req.MAX_RECENT_MERKLE_BLOCK_CACHE = 1000
    bc.req.MAX_RECENT_TX_CACHE = 10000
    bc.req.MAX_RECENT_TX_CACHE = 500

    if (DEFAULT_CHAIN != ChainSelector.NEXA) APP_TITLE = "[" + chainToURI[DEFAULT_CHAIN] + "] " + APP_TITLE

    // Set to use a trusted node that has the tx validation p2p turned on
    if (NEXA_FULL_NODE_ADDR != null)
        activeWallet.blockchain.req.net.exclusiveNodes(setOf(NEXA_FULL_NODE_ADDR!!))

    // Use the "common" identity of the server's wallet as its identity for all communications
    val dest = activeWallet.destinationFor(Bip44Wallet.COMMON_IDENTITY_SEED)
    SERVER_IDENTITY_ADDRESS = dest.address
    RECV_ADDR = activeWallet.getNewAddress().toString()

    // Initialise wallet listener
    initializeWalletListener()
}
