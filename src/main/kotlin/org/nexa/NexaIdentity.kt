package org.nexa

import kotlinx.serialization.Serializable


// Information provided by the Nexa Identity Registration/Information message
// See https://spec.nexa.org/nexid/
@Serializable
data class NexaIdInformation(
    val op: String,
    val addr: String,
    val sig: String,
    val cookie: String,
    val hdl: String? = null,    // handle
    val realname: String? = null,
    val postal: String? = null, // postal address
    val billing: String? = null, // billing address
    val dob: String? = null,    // date of birth
    val ava: String? = null,    // avatar
    val attest: String? = null, // attestations
    val ph: String? = null,     // phone number
    val sm: String? = null      // social media
)