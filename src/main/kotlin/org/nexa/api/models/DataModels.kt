// src/main/kotlin/org/nexa/api/models/DataModels.kt
package org.nexa.api.models
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonObject
import org.nexa.libnexakotlin.Hash256
import java.time.Instant

@Serializable
data class AppConfig(
    val server: ServerConfig,
    val client: Map<String,ClientConfig>,
    val activeClients: Set<String>
)

@Serializable
data class ServerConfig(
    val domainName: String,
    val walletFile: String,
    val blockchain: String,
    val adminAddress: String? = null,
    val adminToken: String? = null,
    val trustedFullNode: String,
    val frontendDevBaseUrl: String,
    val frontendBaseAPIUrl: String,
    val fundAddress: Long = 0,
    val fundAddressCooldown: Int = 30,
)

@Serializable
data class FaucetAddressRequest(
    val address: String
)

@Serializable
data class ClientConfig(
    var location: String,
    var framework: String,
    val routeprefix: String
)
@Serializable
data class WelcomeData(
    val serverFQDN: String,
    val appTitle: String,
    val appDescription: String,
    val sessionId: String?
)

@Serializable
data class SignMessageData(
    val address: String,
    val handle: String,
    val realName: String,
    val postalAddress: String,
    val billingAddress: String,
    val birthday: String,
    val avatar: String,
    val phone: String,
    val socialMedia: String,
    val signature: String,
    val textMessageToSign: String,
    val binaryMessageToSign: String,
    val signTextMessageQrUri: String,
    val signHexBinMessageQrUri: String
)

@Serializable
data class PeerData(
    val logName: String,
    val latency: Double,
    val bytesSent: Long,
    val bytesReceived: Long
)

@Serializable
data class BlockchainData(
    val tipHeight: Long?,
    val tipHash: Hash256?,
    val tipTime: Long?
)

@Serializable
data class WalletDetailsData(
    val p2pPeers: List<PeerData>,
    val blockchain: BlockchainData,
    val walletDump: String
)

@Serializable
data class FaucetData(
    val balance: Long,
    val balanceInCurrency: String,
    val syncedHeight: Long,
    val currentHeight: Long,
    val numPeers: Int,
    val requestQrUri: String,
    val receiveAddress: String,
    val bip21Uri: String
)

@Serializable
data class QrCodeData(
    val title: String,
    val subtitle: String,
    val qrURI: String?
)

@Serializable
data class IdentityData(
    val registerUri: String,
    val loginUri: String
)

@Serializable
data class ReverseQRData(
    val clipboardQrUri: String,
    val nexaAddressQrUri: String
)

@Serializable
data class TDPPData(
    val regUri: String,
    val sendUri1: String,
    val sendUri2: String
)

data class FaucetUsage(
    var totalClaimed: Long,
    var lastClaim: Instant,
    var cryptoAddress: String,
    var lastCooldown: Long,
)

@Serializable
data class ErrorResponse(
    val errors: JsonObject?,
    val message: String,
)

@Serializable
data class SuccessResponse(
    val message: String,
    val data: JsonObject
)