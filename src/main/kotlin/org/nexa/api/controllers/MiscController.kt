// src/main/kotlin/org/nexa/api/controllers/MiscController.kt
package org.nexa.api.controllers

import java.util.logging.Logger

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import org.nexa.*
import org.nexa.SharedResources.activeWallet
import org.nexa.libnexakotlin.*

private val LogIt = Logger.getLogger("nexa.session")


/**
 * Controller for handling miscellaneous endpoints.
 */


/**
 * Processes transaction-related GET requests.
 * Handles the transaction response from the wallet, verifies the transaction, and sends the transaction if valid.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun processTransaction(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    if (cookie != null) {
        val session = sessionFromCookie(cookie)
        if (session == null) {
            call.respondText(tr("unknown session"), status = HttpStatusCode(404, tr("unknown session")))
        } else {
            session.log("wallet responded to faucet tx")
            val txHex = call.parameters["tx"]
            val fTx = session.faucetTx
            if ((txHex != null) && (fTx != null)) {
                val txReturned = txFor(fTx.chainSelector, BCHserialized(txHex.fromHex(), SerializationType.NETWORK))
                if (txReturned.outputs.size == fTx.outputs.size) {
                    var cheater = false
                    for (outputs in txReturned.outputs.zip(fTx.outputs)) {
                        if (outputs.first.amount != outputs.second.amount) {
                            cheater = true
                            break
                        }
                    }
                    if (cheater) {
                        call.respondText(tr("cheater"), status = HttpStatusCode(201, tr("cheater")))
                    } else {
                        val wal = activeWallet
                        try {
                            wal.txCompleter(txReturned, 0, TxCompletionFlags.FUND_NATIVE or TxCompletionFlags.SIGN)
                            LogIt.info(txReturned.toHex())
                            wal.send(txReturned)
                            call.respondText(tr("ok"), status = HttpStatusCode(200, tr("ok")))
                        } catch (e: WalletNotEnoughBalanceException) {
                            launch { session.log("faucet is drained") }
                            call.respondText(tr("faucet is drained"), status = HttpStatusCode(200, tr("faucet drained")))
                        }
                    }
                } else {
                    call.respondText(tr("naaa"), status = HttpStatusCode(200, tr("naaa")))
                }
            } else {
                call.respondText(tr("tx hex decode error"), status = HttpStatusCode(200, tr("bad tx hex")))
            }
        }
    }
}






