// src/main/kotlin/org/nexa/api/controllers/IdentityController.kt
package org.nexa.api.controllers

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import kotlinx.serialization.json.Json
import org.nexa.*
import org.nexa.api.models.IdentityData
import io.ktor.websocket.*
import org.nexa.SharedResources.activeWallet
import org.nexa.api.models.QrCodeData
import org.nexa.libnexakotlin.*
import org.nexa.org.nexa.utils.verifyChallengeTransaction
import org.nexa.plugins.WebSocketManager
import java.util.logging.Logger
import kotlin.text.toByteArray

/**
 * Controller for handling identity-related endpoints.
 */

private val LogIt = Logger.getLogger("nexa.session")

/**
 * Retrieves identity-related data, including URIs for registration and login.
 * Responds with an [IdentityData] object containing the relevant information.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun getIdentityQRData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val qrCodeDataList = mutableListOf<QrCodeData>()

    val registerUri = "nexid://${SERVER_FQDN}/_identity?chal=${session.identityChallenge}&op=reg&cookie=${session.sessionId!!.id}&hdl=m&proto=http&realname=o"
    val loginUri = "nexid://${SERVER_FQDN}/_identity?chal=${session.identityChallenge}&op=login&cookie=${session.sessionId!!.id}&proto=http"
    val registerPostalUri = "nexid://${SERVER_FQDN}/_identity?chal=${session.identityChallenge}&op=reg&cookie=${session.sessionId!!.id}&hdl=m&proto=http&realname=m&postal=m"
    qrCodeDataList.add(
        QrCodeData(
            title = "Information / Registration / Login",
            subtitle = "Requiring a username, and optionally a real name",
            qrURI = registerUri
        )
    )

    qrCodeDataList.add(
        QrCodeData(
            title = "Information / Registration / Login",
            subtitle = "During a purchasing process a site might offer a QR code to easily fill out fields such as postal address",
            qrURI = registerPostalUri
        )
    )

    qrCodeDataList.add(
        QrCodeData(
            title = "Login only",
            subtitle = "You must have previously registered",
            qrURI = loginUri
        )
    )

    call.respond(qrCodeDataList)
}

/**
 * Handles identity-related POST requests.
 * Validates the identity registration/information request, verifies the signature, and updates the session with the received data.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun postIdentity(call: ApplicationCall) {
    val jsonString = call.receiveText()
    val js = Json { ignoreUnknownKeys = true }
    val p = js.decodeFromString<NexaIdInformation>(jsonString)
    val session = sessionFromCookie(p.cookie)
    if (session == null) {
        call.respondText(tr("unknown session"), status = HttpStatusCode(404, tr("unknown session")))
    } else {
        session.log("POST incoming identity request path=${call.request.uri} ")
        session.log("data=$p")
        if (p.op == "reg") {
            session.log("identity registration/information request")
            val signedChallenge = SERVER_FQDN + "_nexid_" + p.op + "_" + session.identityChallenge
            session.log("validating signature='${p.sig}' for address='${p.addr}' on message='$signedChallenge'")
            session.replace("id_addr", p.addr)
            session.replace("id_dob", p.dob ?: "unspecified")
            session.replace("id_handle", p.hdl ?: "unspecified")
            session.replace("id_realname", p.realname ?: "unspecified")
            session.replace("id_postal", p.postal ?: "unspecified")
            session.replace("id_billing", p.billing ?: "unspecified")
            session.replace("id_ava", p.ava ?: "unspecified")
            session.replace("id_attest", p.attest ?: "unspecified")
            session.replace("id_ph", p.ph ?: "unspecified")
            session.replace("id_sm", p.sm ?: "unspecified")
            if (Wallet.verifyMessage(signedChallenge, PayAddress(p.addr), p.sig)) {
                session.log("Signature accepted")
                session.identity = p.addr
                call.respondText(tr("login accepted"), status = HttpStatusCode(200, ""))

                // Broadcast updated identity data over WebSocket
                val json = Json.encodeToString(NexaIdInformation.serializer(), p)
                val message = Frame.Text("Identity updated: $json")
                WebSocketManager.activeSessions.forEach { webSocketSession ->
                    webSocketSession.send(message)
                }
            } else {
                session.log("Signature failure")
                call.respondText(tr("bad signature"), status = HttpStatusCode(200, tr("bad signature")))
            }
        } else {
            session.log("unknown identity request ${p.op}")
        }
    }
}


/**
 * Handles identity-related GET requests.
 * Validates the identity request, verifies the signature, and updates the session with the received data.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun getIdentity(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    if (cookie == null) {
        LogIt.info("bad request, no cookie parameter")
    } else {
        val session = sessionFromCookie(cookie)
        if (session == null) {
            LogIt.info("bad request, stale cookie")
        } else {
            session.log("GET incoming identity request path=${call.request.uri} ")
            val op = call.parameters["op"]
            if (op == "login") {
                session.log("Login request")
                val addr = call.parameters["addr"]
                val sig = call.parameters["sig"]
                val ctxsig = call.parameters["ctxsig"]
                if (addr != null) session.replace("id_address", addr)

                if (addr == null) {
                    session.log("PROTOCOL ERROR: no address provided")
                } else if (sig == null) {
                    if (ctxsig != null) {
                        val challenger = (SERVER_FQDN + "_nexid_" + op).toByteArray()
                        val tx = txFor(activeWallet.chainSelector, BCHserialized(ctxsig.fromHex(), SerializationType.NETWORK))
                        if (verifyChallengeTransaction(session.identityChallenge.toByteArray(), challenger, tx, PayAddress(addr))) {
                            session.log("Challenge transaction signature accepted")
                            call.respondText(tr("login accepted"), status = HttpStatusCode(200, ""))
                            session.identity = addr
                        } else {
                            session.log("Challenge transaction signature failure")
                            call.respondText(tr("bad signature"), status = HttpStatusCode(200, tr("bad signature")))
                        }

                    } else {
                        call.respondText(tr("no signature"), status = HttpStatusCode(200, tr("no signature")))
                        session.log("PROTOCOL ERROR: no signature provided")
                    }
                } else {
                    val signedChallenge = SERVER_FQDN + "_nexid_" + op + "_" + session.identityChallenge
                    session.log("validating signature='${sig}' for address='${addr}' on message='$signedChallenge'")

                    if (Wallet.verifyMessage(signedChallenge, PayAddress(addr), sig)) {
                        session.log("Signature accepted")
                        call.respondText(tr("login accepted"), status = HttpStatusCode(200, ""))
                        session.identity = addr
                    } else {
                        session.log("Signature failure")
                        call.respondText(tr("bad signature"), status = HttpStatusCode(200, tr("bad signature")))
                    }
                }
            }
            if (op == "sign") {
                val addr = call.parameters["addr"]
                val sig = call.parameters["sig"]
                if (addr != null) session.replace("id_addr", addr)
                if (addr != null && sig != null) {
                    if (Wallet.verifyMessage(session.textMessageToSign!!, PayAddress(addr), sig)) {
                        session.replace("id_sig", sig)
                        session.replace("id_sig_title", "signature VALID:")
                        call.respondText(tr("signature accepted"), status = HttpStatusCode(200, ""))
                    } else {
                        // typically you wouldn't have 2 possible messages that might have been signed so we would not need to check both of them
                        if (Wallet.verifyMessage(session.binaryMessageToSign, PayAddress(addr), sig)) {
                            session.replace("id_sig", sig)
                            session.replace("id_sig_title", "signature VALID:")
                            call.respondText(tr("signature accepted"), status = HttpStatusCode(200, ""))
                        } else {
                            session.replace("id_sig", sig)
                            session.replace("id_sig_title", "signature INVALID:")
                            call.respondText(tr("bad signature"), status = HttpStatusCode(200, tr("bad signature")))
                        }
                    }
                }
            }
        }
    }
}
