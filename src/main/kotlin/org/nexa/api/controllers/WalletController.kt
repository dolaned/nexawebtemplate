package org.nexa.api.controllers

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.websocket.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.withTimeout
import org.nexa.*
import org.nexa.SharedResources.activeWallet
import org.nexa.api.models.BlockchainData
import org.nexa.api.models.PeerData
import org.nexa.api.models.WalletDetailsData
import org.nexa.libnexakotlin.*
import org.nexa.plugins.WebSocketManager
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*
import java.util.logging.Logger

private val LogIt = Logger.getLogger("nexa.session")

// Broadcast function
suspend fun broadcastConnectionStatus(statusMessage: String) {
    val message = Frame.Text(statusMessage)
    WebSocketManager.activeSessions.forEach { webSocketSession ->
        try {
            webSocketSession.send(message)
        } catch (e: Exception) {
            LogIt.warning("Failed to send message: ${e.localizedMessage}")
        }
    }
}

// Controller for handling wallet details-related endpoints
suspend fun monitorWalletChanges(wallet: Wallet, transactionHistory: List<TransactionHistory>) {
    // Prepare the status message indicating that the wallet has changed
    val statusMessage = "Wallet has changed"

    // Utilize broadcastConnectionStatus to send the simple status message via WebSockets
    broadcastConnectionStatus(statusMessage)
}

// Setup the wallet change listener
fun setupWalletChangeListener() {
    activeWallet.setOnWalletChange { wallet, transactionHistory ->
        // Launch a coroutine to handle suspend functions
        launch {
            transactionHistory?.let { monitorWalletChanges(wallet = wallet, transactionHistory = it) }
        }
    }
}

// Example usage within the controller, like an init block or a setup method
fun initializeWalletListener() {
    setupWalletChangeListener()
}

// Existing functions like getWalletDetailsData, handleWalletRequest, etc.

suspend fun getWalletDetailsData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val wal = activeWallet
    val wc = wal as CommonWallet
    val cxns = wc.blockchain.net.p2pCnxns

    val p2pPeers = cxns.map {
        PeerData(
            logName = it.logName,
            latency = it.aveLatency,
            bytesSent = it.bytesSent / 1024,
            bytesReceived = it.bytesReceived / 1024
        )
    }

    val bc = wc.blockchain
    val simpleDateFormat = SimpleDateFormat("dd MMMM yyyy, HH:mm:ss", Locale.ENGLISH)
    val ts = bc.nearTip?.time?.let { simpleDateFormat.format(it * 1000L) }

    val blockchain = BlockchainData(
        tipHeight = bc.nearTip?.height,
        tipHash = bc.nearTip?.hash,
        tipTime = bc.nearTip?.time
    )

    val walletDump = wc.debugDump().replace("\n", "<br/>\n")

    val data = WalletDetailsData(
        p2pPeers = p2pPeers,
        blockchain = blockchain,
        walletDump = walletDump
    )
    call.respond(data)
}

suspend fun handleWalletRequest(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    if (cookie == null) {
        LogIt.info("bad request, no cookie parameter")
        call.respondText(tr("invalid request"), status = HttpStatusCode(404, tr("invalid request")))
    } else {
        val session = sessionFromCookie(cookie)
        if (session != null && session.activeWalletConnection == true) {
            val req = call.parameters["req"]
            LogIt.info(sourceLoc() +": Message sent to wallet with session cookie: $cookie, message: $req")
            if (req != null) session.pushToWallet(req)
            call.respondText("{ result: \"ok\"}")
        }
    }
}

suspend fun disconnectWallet(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    if (cookie == null) {
        LogIt.info("bad request, no cookie parameter")
        call.respondText(tr("invalid request"), status = HttpStatusCode(404, tr("invalid request")))
    } else {
        val session = sessionFromCookie(cookie)
        if (session != null) {
            if (session.activeWalletConnection == true) {
                session.pushToWallet("Q")
                session.activeWalletConnection = false
                broadcastConnectionStatus("W0") // Broadcast disconnection
            }
            session.identity = null
            call.respondText("{ result: \"ok\"}")
        }
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
suspend fun handleLongPolling(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    val count = call.request.queryParameters["i"]?.toInt()
    val currentTime = Instant.now()
    if (cookie == null) {
        LogIt.info("bad request, no cookie parameter")
        call.respondText(tr("invalid request"), status = HttpStatusCode.BadRequest)
        return
    }

    val session = sessionFromCookie(cookie)
    if (session == null) {
        LogIt.info("unknown session : $cookie")
        call.respondText(tr("unknown session"), status = HttpStatusCode.NotFound)
        return
    }
    println("last client connection ${session.lastClientConnection}")
    println("last wallet connection ${session.lastWalletConnection}")
    if (count != null) {
        session.walletMessageCount = count
    }
    println("Wallet message count: ${session.walletMessageCount}")

    // If we had no connection, or the wallet thinks we had no connection then update the UI element to show that we have one now
    // It is ok to repeat this update, bad to miss it.
    if ((count != null && count < 1) || (session.activeWalletConnection != true))
    {
        LogIt.info(sourceLoc() +": /_lp: Indicate initial connection for session cookie: $cookie")
        session.activeWalletConnection = true
        session.pushToClient("W1")
        session.lastClientConnection = Instant.now()
        session.lastWalletConnection = Instant.now()
        session.longPollCount++
    } else {
        LogIt.info(sourceLoc() +": /_lp: Indicate sustained connection for session cookie: $cookie")
        session.longPollCount++
        session.lastWalletConnection = currentTime
    }

    if (session.walletLongPoll.isEmpty()) session.walletLongPollHasData.receive(5000L)

    if (!session.walletLongPoll.isEmpty())
    {
        val resp = session.walletLongPoll.removeAt(0)
        LogIt.info(sourceLoc() + ": Wallet long poll for $cookie responding with $resp")
        if (resp == "Q") {
            session.walletLongPoll.clear()
            session.pushToClient("W0")
        }
        call.respondText(resp)
    }
    else
    {
        LogIt.info(sourceLoc() + ": Wallet long poll for $cookie responding with no data")
        call.respondText("")
    }

}
