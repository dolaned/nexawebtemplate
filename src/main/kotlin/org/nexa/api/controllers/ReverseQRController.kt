// src/main/kotlin/org/nexa/api/controllers/ReverseQRController.kt
package org.nexa.api.controllers

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.util.*
import io.ktor.websocket.*
import org.nexa.*
import org.nexa.api.models.QrCodeData
import org.nexa.api.models.ReverseQRData
import org.nexa.plugins.WebSocketManager
import java.util.logging.Logger

/**
 * Controller for handling reverse QR code-related endpoints.
 */

private val LogIt = Logger.getLogger("nexa.session")

/**
 * Retrieves reverse QR code-related data, including URIs for clipboard and Nexa address.
 * Responds with a [ReverseQRData] object containing the relevant information.
 *
 * @param call The ApplicationCall instance.
 * @param session The current application session.
 */
suspend fun getReverseQRData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val qrCodeDataList = mutableListOf<QrCodeData>()

    val clipboardQrUri = "tdpp://${SERVER_FQDN}/share?cookie=${session.sessionId?.id}&info=clipboard&topic=${TDPP_TOPIC}"
    val nexaAddressQrUri = "tdpp://${SERVER_FQDN}/share?cookie=${session.sessionId?.id}&info=address&topic=${TDPP_TOPIC}"

    // Clipboard
    qrCodeDataList.add(
        QrCodeData(
            title = "Clipboard",
            subtitle = "",
            qrURI = clipboardQrUri
        )
    )

    // Nexa Address
    qrCodeDataList.add(
        QrCodeData(
            title = "Nexa Address",
            subtitle = "",
            qrURI = nexaAddressQrUri
        )
    )

    call.respond(qrCodeDataList)
}

/**
 * Handles share-related POST requests.
 * Receives shared data from the wallet and updates the session with the received data.
 *
 * @param call The ApplicationCall instance.
 */
suspend fun handleShare(call: ApplicationCall) {
    val cookie = call.parameters["cookie"]
    if (cookie == null) {
        LogIt.info("bad request, no cookie parameter")
        call.respondText(tr("invalid request"), status = HttpStatusCode(404, tr("invalid request")))
    } else {
        val session = sessionFromCookie(cookie)
        if (session == null) {
            call.respondText(tr("unknown session"), status = HttpStatusCode(404, tr("unknown session")))
        } else {
            session.share = call.receiveText()
            session.replace("reverseQRcontents", session.share.escapeHTML())
            LogIt.info("mobile shared ${session.share}")

            // Broadcast the shared content to all active WebSocket connections
            val message = Frame.Text("Shared content: ${session.share.escapeHTML()}")
            WebSocketManager.activeSessions.forEach { webSocketSession ->
                webSocketSession.send(message)
            }

            call.respondText(tr("ok"), status = HttpStatusCode(200, tr("ok")))
        }
    }
}