// src/main/kotlin/org/nexa/api/controllers/WelcomeController.kt
package org.nexa.api.controllers

import io.ktor.server.application.*
import io.ktor.server.response.*
import org.nexa.*
import org.nexa.api.models.WelcomeData

suspend fun getWelcomeData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val data = WelcomeData(
        serverFQDN = SERVER_FQDN,
        appTitle = "NEXA Wallet Integration Example and Testnet Faucet",
        appDescription = "This example web app shows how a web site can be integrated with the NEXA blockchain and cryptocurrency.",
        sessionId = session.sessionId?.id
    )

    call.respond(data)
}
