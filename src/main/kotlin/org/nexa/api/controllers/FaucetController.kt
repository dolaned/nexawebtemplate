// src/main/kotlin/org/nexa/api/FaucetController.kt
package org.nexa.api.controllers

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.origin
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import org.nexa.*
import org.nexa.SharedResources.activeWallet
import org.nexa.api.models.*
import org.nexa.libnexakotlin.*
import java.net.URLEncoder
import java.time.Instant
import kotlin.random.Random
import org.nexa.org.nexa.utils.getRandomSatoshis
import java.util.logging.Logger

const val MAX_AMOUNT_SATS = 100000000L
const val RESET_TIME = 24 * 60 * 60
const val MIN_REWARD = 500000L
const val MAX_REWARD = 20000000L

private val LogIt = Logger.getLogger("nexa.faucet")
/**
 * Controller for handling faucet-related endpoints.
 */

/**
 * Retrieves faucet-related data, including balance, synced height, and QR codes for testnet coins and returning funds.
 * Responds with a [FaucetData] object containing the relevant information.
 *
 * @param call The ApplicationCall instance.
 * @param session The current application session.
 */
suspend fun getFaucetData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val wal = activeWallet
    val balance = wal.balance
    val syncedHeight = wal.syncedHeight
    val curHeight = wal.blockchain.curHeight
    val numPeers = (wal as CommonWallet).chainstate?.chain?.net?.size ?: 0

    val faucetTx = createFaucetTx(session)
    val requestQrUri = faucetTx?.let {
        val chain = chainToURI[wal.chainSelector]
        val hexTx = it.toHex()
        val uri = "tdpp://${SERVER_FQDN}/tx?chain=${chain}&cookie=${session.sessionId!!.id}&flags=3&inamt=${1 * NEX}&topic=${TDPP_TOPIC}&tx=${hexTx}"
        val sig = wal.signMessage(uri, SERVER_IDENTITY_ADDRESS)
        uri + "&sig=" + URLEncoder.encode(sig, "UTF-8")
    } ?: ""

    val receiveAddress = RECV_ADDR
    val bip21Uri = "$RECV_ADDR?amount=1234567.89&label=faucet"

    val data = FaucetData(
        balance = balance,
        balanceInCurrency = displayCurrency(balance),
        syncedHeight = syncedHeight,
        currentHeight = curHeight,
        numPeers = numPeers,
        requestQrUri = requestQrUri,
        receiveAddress = receiveAddress,
        bip21Uri = bip21Uri
    )
    call.respond(data)
}

/**
 * Retrieves faucet-related QR code data for testnet coins and returning funds.
 * This function is typically used to provide data for QR code generation.
 *
 * @param session The current application session.
 * @return A list of [QrCodeData] objects representing different QR codes.
 */
suspend fun getFaucetQrData(call: ApplicationCall) {
    val session = sessionFromCookie(call) ?: return

    val qrCodeDataList = mutableListOf<QrCodeData>()

    // Get Testnet Coins
    val faucetTx = createFaucetTx(session)
    if (faucetTx != null) {
        val chain = chainToURI[activeWallet.chainSelector]
        val hexTx = faucetTx.toHex()
        session.faucetTx = faucetTx
        val uri = "tdpp://$SERVER_FQDN/tx?chain=${chain}&cookie=${session.sessionId!!.id}&flags=3&inamt=${1 * NEX}&topic=$TDPP_TOPIC&tx=${hexTx}"
        val sig = activeWallet.signMessage(uri, SERVER_IDENTITY_ADDRESS)
        val signedUri = "$uri&sig=${URLEncoder.encode(sig, "UTF-8")}"
        qrCodeDataList.add(
            QrCodeData(
                title = "Get Testnet Coins",
                subtitle = "",
                qrURI = signedUri
            )
        )
    } else {
        qrCodeDataList.add(
            QrCodeData(
                title = "Get Testnet Coins",
                subtitle = "OFFLINE",
                qrURI = null
            )
        )
    }

    // Return funds
    qrCodeDataList.add(
        QrCodeData(
            title = "Return funds",
            subtitle = "",
            qrURI = RECV_ADDR
        )
    )

    // Return funds via BIP21
    qrCodeDataList.add(
        QrCodeData(
            title = "Return ${displayCurrency(123456789L)} via BIP21",
            subtitle = "",
            qrURI = "$RECV_ADDR?amount=1234567.89&label=faucet"
        )
    )

    call.respond(qrCodeDataList)
}


suspend fun createAddressFaucetTx(call: ApplicationCall)
{
    // Check if feature is disabled
    if (FUND_ADDRESS_AMOUNT == 0L)
    {
        println("Fund address not enabled")
        call.respond(HttpStatusCode.NotFound)
        return
    }

    // Extract IP and address
    val ip = call.request.headers["X-Forwarded-For"] ?: call.request.origin.remoteHost
    val request = call.receive<FaucetAddressRequest>()
    val address = request.address

    // Check cooldown from cookie (client-side enforcement)
    val lastPressTime = call.request.cookies["faucetCooldown"]?.toLongOrNull()
    val now = Instant.now().epochSecond
    if (lastPressTime != null && (now - lastPressTime) < FUND_ADDRESS_COOLDOWN)
    {
        call.respond(
            HttpStatusCode.TooManyRequests,
            ErrorResponse(
                message = "Cooldown required",
                errors = JsonObject(mapOf(
                    "remaining" to JsonPrimitive(FUND_ADDRESS_COOLDOWN - (now - lastPressTime!!))
                ))
            )
        )

        return
    }

    // Existing rate limits (server-side)
    val amountToSend = canClaim(ip, address)
    if (amountToSend == null) {
        call.respond(
            HttpStatusCode.BadRequest,
            ErrorResponse(
                message = "Claim limit exceeded",
                errors = JsonObject(mapOf(
                    "retryAfter" to JsonPrimitive(86400)
                ))
            )
        )
        return
    }

    // Attempt transaction
    val tx = try {
        activeWallet.send(amountToSend, address)
    } catch (e: Exception) {
        LogIt.info("Wallet Send error ${e.message}")
        null
    }

    if (tx == null) {
        call.respondText(Json.encodeToString(
            mapOf(
                "message" to "Transaction could not be sent",
            )
        ), status = HttpStatusCode.BadRequest)
        return
    }

    // Update cooldown cookie
    call.response.cookies.append(
        Cookie(
            name = "faucetCooldown",
            value = now.toString(),
            maxAge = FUND_ADDRESS_COOLDOWN,
            secure = true,
        )
    )
    val explorerBase = if (DEFAULT_CHAIN.toString() == "NEXATESTNET") {
        "https://testnet-explorer.nexa.org/tx/"
    } else {
        "https://explorer.nexa.org/tx/"
    }

    call.respond(
        SuccessResponse(
            message = "Transaction successful",
            data = JsonObject(mapOf(
                "txId" to JsonPrimitive(tx.idem.toHex()),
                "explorerLink" to JsonPrimitive("${explorerBase}${tx.idem}"),
                "amountSent" to JsonPrimitive(displayCurrency(amountToSend))
            ))
        )
    )

}




/**
*  Private helper functions
* */
private fun createFaucetTx(session: AppSession): iTransaction?
{
    val wal: Wallet = activeWallet
    val chain = wal.chainSelector

    val wc = wal as CommonWallet
    val faucet = txFor(wal.chainSelector)

    // Add a payout to an address that the other wallet needs to fill in
    val payout = NexaTxOutput(wc.chainSelector, 1* KEX + Random.nextInt(0,10* KEX.toInt()), SatoshiScript(chain, SatoshiScript.Type.TEMPLATE, OP.C0, OP.TMPL_SCRIPT))
    faucet.add(payout)
    try
    {
        // fund that address but don't sign
        // EDIT: Actually don't fund now, fund when the tx comes back filled
        // wal.txCompleter(faucet, 0,TxCompletionFlags.FUND_NATIVE)
        // actually in this case we could just sign an open-ended payment (signature doesn't cover outputs) because we are giving away the money
        // but this is not a common use case so it is better to go thru the entire protocol.
    }
    catch (e: WalletNotEnoughBalanceException)
    {
        launch { session.log("faucet is drained") }
        return null
    }
    return faucet
}

private fun canClaim(ipAddress: String, nexaAddress: String): Long? {
    val now = Instant.now()
    val amountSats =  getRandomSatoshis(FUND_ADDRESS_AMOUNT)

    LogIt.info("Checking claim eligibility for IP: $ipAddress, NexaAddress: $nexaAddress")
    LogIt.info("Current time: $now, Random amount: $amountSats sats")

    // Check if the Nexa address is already associated with a different IP
    if (addressLimits.containsKey(nexaAddress) && addressLimits[nexaAddress] != ipAddress) {
        LogIt.info("NexaAddress $nexaAddress is already associated with a different IP. Claim denied.")
        return null
    }

    val usage = faucetLimits[ipAddress]
    LogIt.info(now.epochSecond.toString())
    if (usage != null) {
        LogIt.info(usage.lastCooldown.toString())
    }
    LogIt.info(FUND_ADDRESS_COOLDOWN.toString())
    // Check if the IP is in cooldown
    if (usage != null && (now.epochSecond - usage.lastCooldown) < FUND_ADDRESS_COOLDOWN) {
        LogIt.info("IP $ipAddress is in cooldown. Last cooldown: ${usage.lastCooldown}. Claim denied.")
        return null
    }

    when {
        // New user or reset period expired
        usage == null || now.epochSecond - usage.lastClaim.epochSecond > RESET_TIME -> {
            LogIt.info("New user or reset period expired. Allowing claim for IP: $ipAddress, NexaAddress: $nexaAddress")
            faucetLimits[ipAddress] = FaucetUsage(
                totalClaimed = amountSats,
                lastClaim = now,
                cryptoAddress = nexaAddress,
                lastCooldown = now.epochSecond
            )
            addressLimits[nexaAddress] = ipAddress
            LogIt.info("Claim approved. Amount: $amountSats sats")
            return amountSats
        }

        // Existing user within limits
        usage.totalClaimed + amountSats <= MAX_AMOUNT_SATS -> {
            LogIt.info("Existing user within limits. Allowing claim for IP: $ipAddress, NexaAddress: $nexaAddress")
            usage.totalClaimed += amountSats
            usage.lastClaim = now
            usage.lastCooldown = now.epochSecond
            LogIt.info("Claim approved. Amount: $amountSats sats. Total claimed: ${usage.totalClaimed} sats")
            return amountSats
        }

        // User exceeded total limits
        else -> {
            LogIt.info("User exceeded total limits. Claim denied for IP: $ipAddress, NexaAddress: $nexaAddress")
            return null
        }
    }
}


