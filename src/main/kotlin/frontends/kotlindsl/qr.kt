package org.nexa.frontends.kotlindsl

import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix

import java.util.logging.Logger
private val LogIt = Logger.getLogger("nifty.qr")

// Function to create the QR code
fun createQR1(qrData:String, szInPix: Int, oneSym: String? = null, zeroSym: String?=null): String
{
    val matrix:BitMatrix = MultiFormatWriter().encode(qrData, BarcodeFormat.QR_CODE, 0, 0)

    val pixSize:Float = szInPix.toFloat()/matrix.width.toFloat()

    val one = oneSym ?: """<symbol id="qr1" width="$pixSize" height="$pixSize"><circle r="${pixSize/2}" cx="${pixSize/2}" cy="${pixSize/2}" fill="black"/></symbol>"""
    //val one = oneSym ?: """<symbol id="qr1" width="$pixSize" height="$pixSize"><circle r="${pixSize/2}" cx="${pixSize/2}" cy="${pixSize/2}" fill="black"/></symbol>"""
    val zero = zeroSym // Nothing for the white background by default

    val ret = StringBuilder()
    val nl = System.getProperty("line.separator")

    ret.append("""<svg width="${szInPix}" height="${szInPix}" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs> ${one}""")
    if (zero != null) ret.append("""${zero}""")
    ret.append("</defs>")
    ret.append(nl)

    for (y in 0 until matrix.height)
    {
        for (x in 0 until matrix.width)
        {
            val px = x*pixSize
            val py = y*pixSize

            ret.append(if (matrix.get(x, y))
                """<use href="#qr1" x="${px}" y="${py}"/>"""
            else
                if (zeroSym != null) """"<use href="#qr1" x="${(x*pixSize).toInt()}" y="${(y*pixSize).toInt()}"/>""" else "")
        }
        ret.append(nl)
    }

    ret.append("</svg>")

    return ret.toString()
}


// Function to create the QR code
var uniquifierNum = 0
fun createQrSvg(classes: String, qrData:String, maxSzInPix: Int, oneSym: String? = null, zeroSym: String?=null, uniquifier: String?=null, forceSize:Boolean=true): String
{
    val matrix:BitMatrix = MultiFormatWriter().encode(qrData, BarcodeFormat.QR_CODE, 0, 0)

    var pixSizeF:Float = maxSzInPix.toFloat()/matrix.width.toFloat()
    if (pixSizeF < 1.0) pixSizeF = 1.0F

    val uStr = if (uniquifier==null)
    {
        (uniquifierNum++).toString()
    }
    else
    {
        uniquifier
    }

    var pixSize = pixSizeF.toInt()
    var szInPix = pixSize*matrix.width

    //val one = oneSym ?: """<marker id="qr1" viewBox="0 0 $pixSize $pixSize" markerWidth="$pixSize" markerHeight="$pixSize" refX="0" refY="0"><circle r="${pixSize.toFloat()/2.0}" cx="${pixSize/2}" cy="${pixSize/2}" fill="black"/></marker>"""
    val one = oneSym ?: """<marker id="qr1$uStr" viewBox="0 0 $pixSize $pixSize" markerWidth="$pixSize" markerHeight="$pixSize" refX="0" refY="0"><rect width="${pixSize}" height="${pixSize}" style="fill:black; stroke:black"/></marker>"""
    //val one = oneSym ?: """<symbol id="qr1" width="$pixSize" height="$pixSize"><circle r="${pixSize/2}" cx="${pixSize/2}" cy="${pixSize/2}" fill="black"/></symbol>"""
    val zero = zeroSym // Nothing for the white background by default

    val ret = StringBuilder()
    val nl = System.getProperty("line.separator")

    val forceSz = if (forceSize) """width="$szInPix" height="$szInPix"""" else ""
    ret.append("""<svg class="$classes" $forceSz viewBox="0 0 $szInPix $szInPix" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs> ${one}""")
    if (zero != null) ret.append("""${zero}""")
    ret.append("</defs>")
    ret.append(nl)
    ret.append(""" "<path d=" """)

    val line = StringBuilder()
    line.append("M0 0 m")

    var dy = 0.0F
    var dx = 0.0F
    var tx = 0.0F
    var lastX = 0
    for (y in 0 until matrix.height)
    {

        for (x in 0 until matrix.width)
        {
            if (matrix.get(x, y))
            {

                line.append("""%.1f %.1f """.format(dx, dy))
                tx += dx
                dx = pixSize.toFloat()
                dy = 0.0F
                lastX = tx.toInt()
            }
            else
                dx += pixSize
        }
        dy+=pixSize
        dx = -lastX.toFloat()  // Rewind to beginning
    }

    ret.append(line.toString())
    ret.append(nl)
    ret.append(""" " fill="none" stroke="none" marker-mid="url(#qr1$uStr)" />
        """.trimMargin())

    ret.append("</svg>")
    return ret.toString()
}
