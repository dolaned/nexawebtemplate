package org.nexa

import io.ktor.server.application.*
import io.ktor.server.sessions.*
import org.nexa.libnexakotlin.*
import io.ktor.server.request.*
import io.ktor.websocket.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.channels.ClosedSendChannelException
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import java.lang.Thread.sleep
import java.time.Duration
import java.time.Instant
import java.util.logging.Logger
import kotlin.random.Random

private val LogIt = Logger.getLogger("nexa.session")

@Serializable
data class AppSessionId(val id: String)

var DEFAULT_CHAIN = ChainSelector.NEXATESTNET
var ADMIN_IDENTITY_ADDRESS:String? = null
var ADMIN_TOKEN:GroupId? = null

val LONG_POLL_TIMEOUT = 5000L
val WALLET_LONG_POLL_TIMEOUT = 5000L
val WALLET_LONG_POLL_TOLERANCE_TIME = 10000L  // 5 seconds after timeout to long poll again
val DEFAULT_SEARCH_COUNT = 5

const val SESSION_ID_LENGTH = 16
const val ALERT_CLEAR_DELAY = 5000L

// Note asset amount is in the script
data class AssetInfo(val script: SatoshiScript, val outpoint: NexaTxOutpoint, val satoshis: Long, val prevout: NexaTxOutput?, var file: String?, var cacheDir: String?)

// Can't access ktor sessions given an arbitrary cookie, so I need to make my own.
// Also RAM session cookie never times out so bad for release
data class AppSession(
    var sessionId: AppSessionId? = null,
    var currentPage: String? = null,
    var identity: String? = null,  // identity (address) of the session creator, if they logged in via the identity protocol
    var username: String?=null,

    var identityChallengeBa: String? = null,
    var assetChallengeBa: ByteArray? = null,

    var textMessageToSign: String? = null,
    var binaryMessageToSign:ByteArray = byteArrayOf(),

    var txProposal:iTransaction? = null,
    var txContinuation: (suspend (AppSession, iTransaction) -> String)? = null,  // if a tx proposal comes back, call this function with the result of analyzing the returned tx

    val assets: MutableMap<GroupId, AssetInfo> = mutableMapOf(),    // NFTs user claims to own
    // Client page dynamic updates

    var faucetTx: iTransaction? = null,
    var share: String = "",  // what the wallet has shared with the server
    var comm: DefaultWebSocketSession? = null,

    // Front end connection
    var lastClientConnection: Instant = Instant.MIN,
    var longPollHasData:Channel<Boolean>  = Channel(UNLIMITED),
    var longPoll: MutableList<String> = mutableListOf(),  // Long poll dynamic page changes

    // Wallet communication
    var activeWalletConnection:Boolean? = null,  // Null means indeterminate state (just restarted)
    var lastWalletConnection:Instant? = null,
    var walletLongPollHasData:Channel<Boolean>  = Channel(UNLIMITED),
    var walletLongPoll: MutableList<String> = mutableListOf(),// Long poll dynamic page changes
    var chainSelector: ChainSelector = DEFAULT_CHAIN,
    var longPollCount:Int = 0,  // So the web app can show this
    var walletMessageCount:Int = 0
) {

    val admin:Boolean
        get() {
            if (identity != null && identity == ADMIN_IDENTITY_ADDRESS) return true
            ADMIN_TOKEN?.let {
                if (assets[it] != null) return true
            }
            return false
        }

    val identityChallenge: String
        get() {
            identityChallengeBa?.let { return it }
            val randomData = Random.Default.nextBytes(16)
            identityChallengeBa = randomData.toHex()
            return identityChallengeBa!!
        }

    val assetChallenge: ByteArray
        get() {  // Create an asset challenge if one has not already been created
            assetChallengeBa?.let { return it }
            val randomData = Random.Default.nextBytes(8)
            assetChallengeBa = randomData
            return randomData
        }

    fun verifySignedChallengeTx(tx: iTransaction): Boolean {
        val acb = assetChallengeBa ?: return false  // We better have created an asset challenge!
        if (tx.outputs.size != 1) return false
        val scr = tx.outputs[0].script
        val lob = scr.parsed()
        if (!(lob[0] contentEquals OP.RETURN.v)) return false
        if (!(lob[1] contentEquals SERVER_FQDN.toByteArray())) return false
        val chalReceived = (lob[2].filterIndexed { i, e -> i%2==1}).toByteArray()
        if (!(chalReceived contentEquals acb)) return false
        return true
    }

    suspend fun log(s:String) {
        LogIt.info(s)
        comm?.send("L" + s)
    }

    suspend fun replace(cls: String, data: String) {
        comm?.send("R" + cls + "\n" + data)
    }

    suspend fun pushToClient(s:String) {
        comm?.send(s)
    }

    suspend fun pushToWallet(req: String)
    {
        walletLongPoll.add(req)
        walletLongPollHasData.trySend(true)
    }

    // TODO actually require ownership proof rather than wallet's claim
    fun provenOwnershipOf(gid: GroupId): Boolean
    {
        return assets[gid] != null
    }

}

val sessions = mutableMapOf<String,AppSession>()

fun findCreateSession(call: ApplicationCall?): Pair<String,AppSession> {
    var nsi: AppSessionId? = call?.sessions?.get()
    if (nsi == null) {
        nsi = AppSessionId(newCookie(SESSION_ID_LENGTH))
        call?.sessions?.set(nsi)
    }
    var ret = sessions[nsi.id]
    if (ret == null) {
        ret = AppSession(nsi)
        sessions[nsi.id] = ret
    }
    return Pair(nsi.id, ret)
}

fun findSession(call: ApplicationCall?): AppSession? {
    var nsi: AppSessionId? = call?.sessions?.get()
    if (nsi == null) return null
    var ret = sessions[nsi.id]
    return ret
}

fun sessionNewPage(call: ApplicationCall?): Pair<String, AppSession> {
    val (cookie, session) = findCreateSession(call)
    val pagePath = call?.request?.path()
    session.currentPage = pagePath
    return Pair(cookie, session)
}

val cookieChars : List<Char> = ('A'..'Z') + ('0'..'9')
fun newCookie(len: Int=16):String {
    return (1..len).map { cookieChars.random() }.joinToString("")
}

fun checkWalletAlive(session: AppSession?): Boolean
{
    if (session == null) return false
    val now = Instant.now()
    if (session.activeWalletConnection != true) return false
    if (session.lastWalletConnection != null) {
        if (session.lastWalletConnection!! < now - Duration.ofMillis(WALLET_LONG_POLL_TOLERANCE_TIME + WALLET_LONG_POLL_TIMEOUT)) // wallet didn't call _lp within 15 seconds of the last connection
        {
            LogIt.info("Wallet long poll for session ${session.sessionId?.id ?: "[null]"} expired")
            // Assume that wallet connection is lost.  If it reconnects, it'll autochange the indicator
            session.activeWalletConnection = false
            return false
        }
    }

    return true
}

fun walletSessionMonitor()
{
    while(true)
    {
        LogIt.info("Wallet Session monitor checking status of ${sessions.size} sessions")
        sleep(WALLET_LONG_POLL_TOLERANCE_TIME)
        var walletAlive = true;
        for ((_, session) in sessions)
        {
            if (checkWalletAlive(session))
            {
                LogIt.info("Wallet for session ${session.sessionId?.id ?: "[null]"} is alive")
            } else {

                LogIt.info("Wallet for session ${session.sessionId?.id ?: "[null]"} is not alive")
                CoroutineScope(Dispatchers.Default).launch {
                    session.pushToClient("W0")
                }

                walletAlive = false
            }


            // If the web client hasn't checked in for 3 timeout periods then assume that the client session is gone so end the wallet long polling
            if (session.lastClientConnection < Instant.now().minusMillis(LONG_POLL_TIMEOUT * 3))
            {
                if(!walletAlive) {
                    LogIt.info("Deleting inactive session ${session.sessionId}")
                    sessions.removeByValue(session)

                }
            }

        }
    }
}

fun sessionFromCookie(cookie:String):AppSession? {
    val session = sessions[cookie]
    return session
}

fun sessionFromCookie(call: ApplicationCall): AppSession? {
    var c = call.parameters["cookie"]
    if (c == null) return null
    val session = sessions[c]
    return session
}
