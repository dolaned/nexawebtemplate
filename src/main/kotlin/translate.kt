package org.nexa

import org.nexa.SharedResources.activeWallet
import org.nexa.frontends.kotlindsl.defaultCurrencyUnitFormat
import org.nexa.frontends.kotlindsl.finestToDefaultCurrencyUnit
import org.nexa.libnexakotlin.chainToCurrencyCode


// Translate strings
fun tr(s: String?): String
{
    if (s == null) return ""
    return s
}

fun displayCurrency(x: Long): String
{
    val currency: String = chainToCurrencyCode[activeWallet.chainSelector] ?: ""
    return defaultCurrencyUnitFormat.format(finestToDefaultCurrencyUnit(x)) + " " + currency
}