export default {
    mounted(el, binding) {
        el.addEventListener('click', () => {
            const selector = binding.value;
            const element = document.querySelector(selector);
            const navbar = document.getElementById('topbar-nav');
            const navbarHeight = navbar ? navbar.offsetHeight : 0;

            if (element) {
                const elementPosition = element.getBoundingClientRect().top + window.pageYOffset;
                const offsetPosition = elementPosition - navbarHeight;

                window.scrollTo({
                    top: offsetPosition,
                    behavior: 'smooth',
                });
            }
        });
    },
};