/** @type {import('tailwindcss').Config} */
import typography from "@tailwindcss/typography";
export default {
  content: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  safelist: [
    'blue-3xl',
    'rounded-full'
  ],
  theme: {
    extend: {
      colors: {
        primary: '#a855f7',    // Dark blue for primary elements
        secondary: '#ffe144',  // Bright yellow for secondary elements
        accent: '#0077b6',     // Bright blue for accents
        neutral: '#f8f9fa',    // Light grey for neutral tones
        'base-100': '#ffffff', // White background
        'base-200': '#e9ecef', // Light grey for secondary backgrounds
        'base-300': '#dee2e6', // Grey background for tertiary surfaces
        info: '#17a2b8',       // Information color
        success: '#28a745',    // Success color
        warning: '#ffc107',    // Warning color
        error: '#dc3545',      // Error color,
        'dark-color-start': 'hsl(231, 45%, 9%)',
        'dark-color-black': 'hsla(201, 100%, 1%, 1)',
      },
      backgroundImage: {
        'dark-gradient': 'linear-gradient(to bottom, var(--tw-gradient-stops))',
      },
      borderRadius: {
        'box': '0.25rem',  // Matching --rounded-box
        'btn': '0.25rem',  // Matching --rounded-btn
      },
      fontFamily: {
        'sans': ['"Open Sans"', 'sans-serif'],
        'roboto': ['Roboto', 'sans-serif'],
      },
      blur: {
        '3xl': '64px',
        '4xl': '80px',
      },
      spacing: {
        '120': '30rem',
        '1.25': '0.3125rem'
      },
    }
  },
  variants: {
    extend: {
      display: ['dark'], // Enable dark mode for displays
      backgroundImage: ['dark'], // Enable dark mode for background images
    },
  },
  plugins: [typography],
};


