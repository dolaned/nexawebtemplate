import ClickOutside from '@/directives/clickOutsideDirective'
import ScrollToElement from '@/directives/scrollToElementDirective'

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.directive('click-outside', ClickOutside)
    nuxtApp.vueApp.directive('scroll-to-element', ScrollToElement)
})
