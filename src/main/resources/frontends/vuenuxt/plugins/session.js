import { ref } from 'vue'

export default defineNuxtPlugin(async (nuxtApp) => {
    const config = useRuntimeConfig()
    // Initialize reactive references
    const serverFqdn = ref('127.0.0.1') // Ensure this is set correctly
    const sessionId = ref('')
    const session = ref({})
    console.log(config)

    // Define the function to fetch session data
    const fetchSession = async () => {
        try {
            const data = await $fetch(`/session`)
            serverFqdn.value = data.SERVER_FQDN
            sessionId.value = data.sessionId
            session.value = data
        } catch (error) {
            console.error('Error fetching session data:', error)
        }
    }

    // Fetch session data initially
    await fetchSession()

    // Make the reactive references available globally
    nuxtApp.provide('session', session)
    nuxtApp.provide('serverFqdn', serverFqdn)
    nuxtApp.provide('sessionId', sessionId)

    // Provide fetchSession function for manual refresh if needed
    nuxtApp.provide('fetchSession', fetchSession)
})
