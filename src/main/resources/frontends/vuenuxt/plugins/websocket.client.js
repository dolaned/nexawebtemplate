import mitt from 'mitt'

export default defineNuxtPlugin((nuxtApp) => {
    const config = useRuntimeConfig()
    const emitter = mitt()  // Event emitter for WebSocket events
    let socket = null
    let reconnectAttempts = 0
    const maxReconnectAttempts = 5
    let w1Timeout = null
    let connectionAlive = true;
    const PING_INTERVAL = 15000; // 15 Seconds

    const connectWebSocket = () => {
        const { $sessionId, $serverFqdn } = nuxtApp

        if (!$sessionId.value || !$serverFqdn.value) {
            console.error('Session ID or server FQDN not available')
            return
        }

        const socketUrl = `/ws?cookie=${$sessionId.value}`

        socket = new WebSocket(socketUrl)

        socket.onopen = () => {
            console.log('WebSocket connection established')
            reconnectAttempts = 0
            emitter.emit('open')

            setInterval(() => {
                if(connectionAlive === false) {
                    emitter.emit('timeout')
                }
                connectionAlive = false
                console.log('wallet session cookie: ' + $sessionId.value)
                console.log('sending ping')
                socket.send('ping')
            }, PING_INTERVAL)
        }

        socket.onmessage = (event) => {
            console.log('Message from server:', event.data)
            emitter.emit('message', event)  // Emit message event with the event object

            const data = event.data.trim()

            if (data === 'W1') {
                clearTimeout(w1Timeout)
                emitter.emit('W1')
            } else if (data.startsWith('Shared content: ')) {
                const address = data.substring('Shared content: '.length)
                emitter.emit('address', address)
                console.log('Wallet address received:', address)
            }
        }

        socket.onclose = () => {
            console.log('WebSocket connection closed')
            emitter.emit('close')
            attemptReconnect()
        }

        socket.onerror = (error) => {
            console.error('WebSocket error:', error)
            emitter.emit('error', error)
            attemptReconnect()
        }

        nuxtApp.provide('socket', socket)
        nuxtApp.provide('socketEmitter', emitter)  // Provide the event emitter
    }

    const attemptReconnect = () => {
        if (reconnectAttempts < maxReconnectAttempts) {
            reconnectAttempts++
            console.log(`Reconnecting... Attempt ${reconnectAttempts}`)
            setTimeout(connectWebSocket, 2000)  // Reconnect after 2 seconds
        } else {
            console.error('Max reconnect attempts reached')
        }
    }

    connectWebSocket()
})
