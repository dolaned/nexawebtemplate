export default defineNuxtPlugin((nuxtApp) => {
    const { $sessionId, $serverFqdn, $socket } = nuxtApp
    let isConnected = false
    let walletAddress = ''

    const isWalletConnected = () => {
        return isConnected
    }

    const connectWallet = () => {
        isConnected = true
    }

    const disconnectWallet = async () => {
        try {
            await $fetch(`/api/wallet/disconnect?cookie=${$sessionId.value}`)
            // The address will be received via WebSocket
        } catch (error) {
            console.error('Error disconnecting wallet:', error)
        }
        // Actual disconnecting logic can be implemented if it exists
        isConnected = false
        walletAddress = ''
    }

    const setAddress = (address) => {
        walletAddress = address
    }

    const getWalletAddress = () => {
        return walletAddress
    }

    const fetchWalletAddress = async () => {
        const req = `tdpp://${$serverFqdn.value}/share?cookie=${$sessionId.value}&info=address&topic=example`
        try {
            await $fetch(`/_walletRequest?cookie=${$sessionId.value}&req=${encodeURIComponent(req)}`)
            // The address will be received via WebSocket
        } catch (error) {
            console.error('Error fetching wallet address:', error)
        }
    }

    const qrClickHandler = async (uri) => {
        if (isWalletConnected()) {
            try {
                await $fetch(`/_walletRequest?cookie=${$sessionId.value}&req=${encodeURIComponent(uri)}`)
                // The signed message will be received via WebSocket
            } catch (error) {
                console.error('Error fetching signed message response:', error)
            }
        }
    }

    nuxtApp.provide('wallet', {
        isWalletConnected,
        connectWallet,
        disconnectWallet,
        setAddress,
        getWalletAddress,
        fetchWalletAddress,
        qrClickHandler
    })
})
