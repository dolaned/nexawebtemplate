
let socket = new WebSocket("ws://" + location.host + "/ws");
walletCnxn = false;

socket.onopen = function(e)
{
};

// https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
function setCookie(name,value,days)
{
    var expires = "";
    if (days)
    {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    let cookieval = name + "=" + (value || "")  + expires + "; path=/; SameSite=Strict";
    console.log("set cookie:" + cookieval)
    document.cookie = cookieval;
}
function getCookie(name)
{
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name +'=; Path=/; SameSite=Strict; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}


function log(s)
{
    let logElem = document.getElementsByClassName("log")[0];
    if (logElem != null)
    {
        let newLog = document.createElement("div");
        newLog.appendChild(document.createTextNode(s));
        logElem.append(newLog);
    }
}

socket.onmessage = function(event)
{
    let t = event.data;
    if (t[0] == "L") // log
    {
        log(t.slice(1));
    }
    else if (t[0] == "R")  // Replace children of every element that matches this class name
    {
        t = t.slice(1);
        let s = t.split("\n");
        let cls = s[0];
        let text = s[1];
        let elems = document.getElementsByClassName(cls);
        for (i=0; i<elems.length; i++)
        {
            let elem = elems[i];
            elem.innerHTML = text;
        }
    }
    else if (t[0] == "W")
    {
        if (t[1] == "1") walletCnxn = true;
        else if (t[1] == "0") walletCnxn = false;
    }
};

socket.onclose = function(event)
{
  if (event.wasClean)
  {
  }
  else
  {
  }
};

socket.onerror = function(error)
{
};

function showInfo(textToShow)
{
    let infoElem = document.getElementsByClassName("info")[0];
    infoElem.innerHTML = textToShow;
}

function brighten(elem)
{
    elem.classList.remove("dim");
    elem.classList.add("bright");
}

function dim(elem)
{
    elem.classList.remove("bright");
    elem.classList.add("dim");
}

function dimAll(className)
{
    let QRs = document.getElementsByClassName("QRbutton");
    for(i=0;i<QRs.length; i++)
        dim(QRs[i]);
}

function highlightMenuItem(elem)
{
    elem.classList.add("tabItemHighlight");
}

function unhighlightMenuItem(elem)
{
    elem.classList.remove("tabItemHighlight");
}

function restoreTab(tabGroupClassName)
{
    let curtab = getCookie(tabGroupClassName+"_curTab");
    console.log("cookie is " + curtab);
    if (curtab != null)
    {
        console.log("set curtab " + curtab)
        focusTab(tabGroupClassName, curtab);
    }
}

function focusTab(tabGroupClassName, tabIdentity)
{
    let tabs = document.getElementsByClassName(tabGroupClassName);
    for(i=0;i<tabs.length; i++)
    {
        let tab = tabs[i];
        if (tab.id === tabIdentity)
        {
            setCookie(tabGroupClassName+"_curTab", tabIdentity, 7);
            let disp = tab.getAttribute("data-actual-display");
            if (disp == null || disp === "" || disp === "none")
            {
                if (tab.style.display != "none" ) disp = tab.style.display;
                else disp = "block";
            }
            tab.style.display = disp;
        }
        else
        {
            let disp = tab.getAttribute("data-actual-display");
            if (disp == null)  // 1st time through record the original display type
            {
                let t = tab.style.display;
                if (t == null || t === "" || t === "none") t = "block";
                tab.setAttribute("data-actual-display", t);
            }
            tab.style.display = "none";
        }
    }
}

async function walletRequest(req)
{
    log("request server to contact wallet: " + req);
    const response = await fetch(
        '/_walletRequest?req=' + encodeURIComponent(req),
        {
            method: 'GET'
        });
    const json = await response.text();
    //console.log("received: ", json);
    log("server response: " + json);
}

function buttonClicked(uri)
{
  if (walletCnxn)
  {
    walletRequest(uri)
  }
  else
  {
      window.open(uri,'_blank');
  }
}

async function signMessageChanged(elem)
{
    let text = elem.value;
    // console.log(text);
    const response = await fetch("/_changeSignMessage?msg=" + encodeURIComponent(text), { method: "GET"});
    const newtext = await response.text();
    //console.log(newtext);
    let rep = document.getElementsByClassName("signTextMessageQrButton");
    for (let i = 0; i < rep.length; i++)
    {
        rep[i].innerHTML = newtext;
    }
}


async function signMessageChangedHex(elem)
{
    let text = elem.value;
    // console.log("hex to sign:", text);
    const response = await fetch("/_changeSignMessage?msghex=" + encodeURIComponent(text), { method: "GET"});
    const newtext = await response.text();
    //console.log(newtext);
    let rep = document.getElementsByClassName("signHexBinMessageQrButton");
    for (let i = 0; i < rep.length; i++)
    {
        rep[i].innerHTML = newtext;
    }
}


setTimeout(function() {
    restoreTab("tabGroup");
    signMessageChanged(document.getElementById("whatToSign"));
    signMessageChangedHex(document.getElementById("whatToSignHex"));
    console.log("loaded sign box contents");
    let bodies = document.getElementsByTagName("body");
    for (let i = 0; i < bodies.length; i++) bodies[i].style.display="block";
    }, 10);

console.log("loaded");