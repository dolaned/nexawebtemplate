# Nexa Faucet and Example Web App

This is an example web app that provides faucet, identity, and payment functionality, demonstrating integration with client wallets (currently Wally) via the Nexa Identity and Delegated Payment Protocol functionality.  To accomplish the faucet functionality, the web app is running its own Nexa wallet.

It relies on the Nexa Kotlin/Java library jars/libnexajvm.jar (https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin, "nexa" branch) which in turn uses libnexa.so from the Bitcoin Unlimited Nexa full node project (https://gitlab.com/nexa/nexa).  For your convenience, Linux Intel 64 bit versions of these libraries have been committed to this repository.  However, before using this code on mainnet Nexa, you should build these libraries yourself.

## Key Functionality Demonstrated

This example demonstrates important functionality needed to implement the Nexa UX Architecture:

* User's light client (mobile wallet) acting as the user's trusted "agent" or "intermediary" when interacting with Nexa-enabled services.
* Ability to scan QR codes to push secure operations to the light client.  This allows the site to have no login and little security, but execute financial operatons.
* Establishing independent but interconnected sessions between both the user's light client and the user's browser, via QR code.
* Using these interconnected sessiosn to have an action in the browser/web app directly push a request to the user's light client without requiring a QR code scan.


## Structure

This project is divided into back end routes implemented in Kotlin, and front end routes / client-side UX implemented in a variety of front end technologies.  
All front ends are located at [src/main/resources/frontends](src/main/resources/frontends), and server-side code that serves front end UX logic is located at [src/main/kotlin/frontends](src/main/kotlin/frontends).  

There is a configuration tool "FrontendInitializer" (see below) that allows you to create new "stub" front ends.  
However, you can also just make a new subdirectory below "frontends", add your routes into [FrontEndRoutes.kt](src/main/kotlin/org/nexa/setup/FrontEndRoutes.kt), and change "app.cfg" to enable your new front end.

You may have multiple front-ends within the same project.  You can even run them simultaneously by changing FrontEndRoutes.kt.

By default, the project is configured to run a simple kotlin DSL front end "out-of-the-box".

**TODO: conditionally remove the src/main/kotlin/xxx dirs based on what front ends are not selected**

## Development

This is an Intellij IDEA project, and its tested/supported on Intel x64 Ubuntu linux.  You do not have to use this platform and IDE software, but if you are just getting started it will save you a lot of pain.

Download the Intellij IDEA here: https://www.jetbrains.com/idea/download/#section=linux

### Simple Configuration and Startup

 * Create/change app.cfg to match your configuration
 ```
{
    "server" : {
        "domainName" : "<your IP or FQDN>",
        "walletFile" : "appWallet",
        "blockchain" : "nexatest",
        "trustedFullNode" : "127.0.0.1"
    }
    "client": {
        "location": "src/main/resources/frontends/kotlindsl",
        "framework": "kotlindsl"
    }
}
``` 

*domainName*  **MUST** be a real IP, not 127.0.0.1 because the user's mobile wallet must be able to independently contact the site at this address.  If you use a subnet behind a NAT, you must make sure that mobile devices are connected to that subnet.

*trustedFullNode* is optional -- if you use it, the wallet will talk exclusively to the Nexa full node at the provided IP address.  This is recommended for deployment.  Its also nice for development so you can see transactions being posted to the mempool and/or use regtest.

With the "kotlindsl" configuration the project should build and display basic pages, demonstrating the key functionality.

### Logging

Logs are located in "dwally.log.*".


## Deployment

This section is pretty specific to how I am deploying this to testnetfaucet.nexa.org.  In essence, I am running this server via the "pm2" monitoring system on its own TCP port.  Then I set up a virtual server in apache2 to forward websocket and http requests to this port.

You may choose other techniques and HTTP stacks.

### Build

```
./gradlew distZip
```

Distribution is: build/distributions/org.nexa.nexawebtemplate-0.0.3.zip 

### Dependencies

```
~# apt install openjdk-17-jre-headless
```

## Install and Run

Note that you can also just create a tmp dir in your local machine and unzip there to test the installation.  
Remember to copy (and possibly edit) the app.cfg file from the project root directory into your test tmp dir!

```
scp build/distributions/org.nexa.nexawebtemplate-0.0.3.zip  buwiki@testnet-faucet.nexa.org:~

ssh buwiki@testnet-faucet.nexa.org
cd /opt
unzip -o ~/org.nexa.nexawebtemplate-0.0.3.zip
```


*first time*
Create config file org.nexa.nexawebtemplate-0.0.3/bin/app.cfg:
```
{
    "server" : {
        "domainName" : "<your IP or FQDN>",
        "walletFile" : "appWallet",
        "blockchain" : "nexatest",
        "trustedFullNode" : "127.0.0.1"
    }
}
```

Make a symlink for easy access
```
ln -s org.nexa.nexawebtemplate-0.0.3/bin/org.nexa.nexawebtemplate nwt
```

*or just execute*
```
cd org.nexa.nexawebtemplate-0.0.3/bin; ./org.nexa.nexawebtemplate
```

Browse to localhost:7997 to see the site.

*deploy*
```
cd /opt/testnetexample/bin
pm2 start ./org.nexa.nexawebtemplate --name "testnetexample" --interpreter=/bin/bash
pm2 save
```

*enable https*
https://certbot.eff.org/
```
sudo certbot --apache
```

*redeploy*
```
pm2 restart testnetexample
```

### Apache2 modules

a2enmod rewrite
a2enmod proxy_wstunnel
a2enmod proxy_http
systemctl restart apache2

### Apache2 /etc/apache2/sites-enabled/000-default.conf configuration

<VirtualHost *:80>
        ServerName testnetfaucet.nexa.org

       RewriteEngine On
       RewriteCond %{HTTP:Upgrade} =websocket [NC]
       RewriteRule /(.*)           ws://localhost:7997/$1 [P,L]
       RewriteCond %{HTTP:Upgrade} !=websocket [NC]
       RewriteRule /(.*)           http://localhost:7997/$1 [P,L]


       ProxyPass / http://127.0.0.1:7997/
       ProxyPassReverse / http://127.0.0.1:7997/
       ErrorLog ${APACHE_LOG_DIR}/error.log
       CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

# FrontendInitializer

## Overview

The `FrontendInitializer` is a Kotlin script designed to automate the creation of frontend projects using popular frameworks such as React and Vue. It provides a streamlined process to initialize, install dependencies, build the project, and update the backend configuration automatically.

One of the frontend options includes a Nuxt Boilerplate which has various examples of interacting with the Nexa blockchain via the Kotlin backend. The repo for this boilerplate can be found here: [nexa-webtemplate-nuxt-boilerplate](https://gitlab.com/myendy/nexa-webtemplate-nuxt-boilerplate). 
## Prerequisites

Before using the `FrontendInitializer`, ensure you have the following tools installed on your system if you intend on using React or Vue templates:

- **Node.js**: Download and install from [nodejs.org](https://nodejs.org/). Minimum version: `v18.0.0`
- **npm**: This comes with Node.js, but you can check the version using `npm -v`.
- **npx**: This also comes with Node.js, but you can check the version using `npx -v`.

The current minimum supported Node.js version for Vue (Nuxt.js) and React (Next.js) is: `v18.0.0`

It is recommended to obtain the latest version from [nodejs.org](https://nodejs.org/en)

This is required for React and Vue development.

If you do not intend on using React or Vue, you can skip installing these.


## Usage

### Running the Script

1. **Open a Terminal in IntelliJ or your preferred IDE**.

2. **Ensure the correct Node.js version is used (if using Vue or React)**:
   Make sure you are using the correct Node.js version managed by `nvm`. You can check the version using:
   ```bash
   node -v
   ```

3. **Run the Kotlin Script**:
    Within IntelliJ the source tree to src/main/kotlin/setup/FrontendInitializer.kt, 
    and run the main() function by clicking on the green triangle near main's definition.
    Running this within the IDE will allow the IDE to set up all the dependency classpath's automatically.

    TODO: Execute the script from the terminal.

   Read more about running applications in Jetbrains IDEs [here](https://www.jetbrains.com/help/idea/running-applications.html)

### Interactive Setup

The script will prompt you for input to configure your new frontend project.

1. **Node.js and npm Versions**:
   The script will check and display the current versions of Node.js and npm installed on your system.

   ```plaintext
    Node.js version: v20.16.0
    
    npm version: 10.8.1
   ```

2. **Enter the Project Name**:
   You will be prompted to enter the name of your new frontend project.

   ```plaintext
   Enter the project name:
   ```

3. **Choose a Framework**:
   The script will present a list of supported frameworks. Enter the number corresponding to your desired framework.

   ```plaintext
   1. React (Next.js)
   2. Vue (Nuxt.js)
   3. Vue (Nuxt.js) - Faucet Boilerplate
      ```

### Example Session

Below is an example session. This session is started after clicking "Run" on the `FrontendInitializer.kt` script inside your IDE:

```plaintext
Node.js version: v20.16.0

npm version: 10.8.1

Enter the project name: my-frontend-app

1. React (Next.js)
2. Vue (Nuxt.js)
3. Vue (Nuxt.js) - Faucet Boilerplate
Choose a framework: 1

React project my-frontend-app initialized successfully.

Installing dependencies...
...

Building the project...

Project built successfully.

app.cfg updated with location and framework for my-frontend-app.
```

### Script Functions

1. **Initialize Frontend Project**:
   The script creates a new project directory and runs the appropriate CLI command to set up the project. If a project with the same name already exists, it prompts the user to delete it.

2. **Install Dependencies**:
   The script installs the project dependencies using `npm install`.

3. **Build Project**:
   The script builds the project using the appropriate build commands for the selected framework. For Vue, it runs both `npm run build` and `npm run generate`.

4. **Update Configuration**:
   The script updates the `app.cfg` file with the location and framework details of the new frontend project. If the `client` section is missing from the `app.cfg`, it prompts the user to add it.
   The script will also create an .env file per project with `BASE_URL` and `API_BASE_URL` for easy connection to the Kotlin backend.
   **TODO: Explain what these need to be set to so that a dev can hack them**
## Routing Configuration

Once the frontend project has been created, frontend routes will be injected into the Routing.kt file when the application is built and ran.
### Routing.kt

```kotlin
package org.nexa.plugins

import io.ktor.application.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import java.io.File

fun Application.module() {
    routing {

        /* FRONTEND-ROUTES */
        frontendRoutes()
    }
}
```

### FrontendRoutes.kt

```kotlin
package org.nexa.plugins

import io.ktor.server.http.content.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.json.Json
import org.nexa.setup.AppConfig
import java.io.File

const val CONFIG_FILE = "app.cfg"

fun Route.frontendRoutes() {
    val cfg = File(CONFIG_FILE).readText(Charsets.UTF_8)
    val appConfig = Json.decodeFromString<AppConfig>(cfg)

    val projectName = appConfig.client.location.substringAfterLast("/")
    val framework = appConfig.client.framework

    when (framework.lowercase()) {
        "reactnext" -> {
            staticFiles("/_next", File("src/main/resources/frontends/$projectName/out/_next"))
            staticFiles("/", File("src/main/resources/frontends/$projectName/out"))
            staticFiles("/_next/static", File("src/main/resources/frontends/$projectName/out/_next/static"))
            get("/") {
                call.respondFile(File("src/main/resources/frontends/$projectName/out/index.html"))
            }
        }
        "vuenuxt" -> {
            staticFiles("/_nuxt", File("src/main/resources/frontends/$projectName/dist/_nuxt"))
            staticFiles("/", File("src/main/resources/frontends/$projectName/dist"))
            get("/") {
                call.respondFile(File("src/main/resources/frontends/$projectName/dist/index.html"))
            }
        }
        "nuxt-faucet" -> {
            // Serve static files for a Nuxt-Faucet project
            staticFiles("/_nuxt", File("src/main/resources/frontends/$projectName/dist/_nuxt"))
            staticFiles("/", File("src/main/resources/frontends/$projectName/dist"))

            // Serve the Nuxt-Faucet index.html file at the root URL
            get("/") {
                call.respondFile(File("src/main/resources/frontends/$projectName/dist/index.html"))
            }
        }
        "kotlindsl" -> {
            // This is just an example, you can call kotlin functions, and they will be injected into Routing.kt
            get("/") {
                homePage(call)
            }
            staticFiles("/static", File("src/main/resources/static"))
        }
        "custom" -> {
            // Add whatever you want here
        }
        else -> throw IllegalArgumentException("Unsupported framework: $framework")
    }
}

```
### Example `app.cfg` Client Configuration

```json
{
  "server": {
      "domainName": "192.168.1.0:7997", 
      "walletFile": "appWallet", 
      "blockchain": "nexatest", 
      "trustedFullNode": "127.0.0.1", 
      "frontendDevBaseUrl": "http://localhost:3000", 
      "frontendBaseAPIUrl": "localhost:7997"
    },
  "client": {
    "kotlindsl": {
      "location": "src/main/resources/frontends/kotlindsl", 
      "framework": "kotlindsl", 
      "routeprefix": "/kdsl"
    },
    "vuenuxt": {
      "location": "src/main/resources/frontends/vuenuxt", 
      "framework": "vuenuxt", 
      "routeprefix": "/"
    }
  },
  "activeClients": [
    "kotlindsl", 
    "vuenuxt"
  ]
}
```

### Framework-Specific Entry Points

- **React (Next.js)**:
  - Static files directory: `/out/.next`
  - Root directory: `/out`
  - Entry point: `/out/index.html`


- **Vue (Nuxt.js)**:
    - Static files directory: `/dist/_nuxt`
    - Root directory: `/dist`
    - Entry point: `/dist/index.html`


- **Vue (Nuxt.js) - Faucet Boilerplate**:
    - Static files directory: `/dist/_nuxt`
    - Root directory: `/dist`
    - Entry point: `/dist/index.html`


- **Kotlin**:
    - Static files directory: `/static`
    - Entry point: `homePage()`


- **Custom**:
    - Static files directory: `any/folder/you/like`
    - Root directory: `/anywhere`
    - Entry point: `anyFunctionYouLike() or /any/entrypoint.html`

## Configuration

Any frontend that provides a solution for generating HTML and JS can technically be used with this project. You can also choose to use Kotlin with the `kotlin` or `custom` option in `client` configuration in app.cfg.
This script does not need to be used in order to create a frontend, as long as `app.cfg` points to the correct location and `FrontendRoutes.kt` is adjusted for your solution, you're good to go.
## Troubleshooting

### CLI Not Installed

If the required CLI tool is not installed, the script will provide a message on how to install it.

```plaintext
The required CLI tool is not installed. Please install it and try again.
You can install it using the following commands:

React (Next.js): npm install -g create-next-app
Vue (Nuxt.js): npm install -g nuxi
Nuxt-Faucet: Ensure git is installed on your system

Make sure you have Node.js and npm installed. You can download them from https://nodejs.org/
```

### Environment Issues

Ensure that your environment is correctly set up with the necessary tools and that they are included in your system's PATH.


# Building the Vue/Nuxt front end

Use the FrontendInitializer.kt described above initialize the front end.  For the purposes of this example, assume it is located in "vuenuxt" (src/main/resources/frontends/vuenuxt")

Head over to the sub-project:
```bash
cd src/main/resources/frontends/vuenuxt
```

Install front-end dependencies (once):
```bash
npm install
```

Actually build:
```bash
npm run build; npm run generate
```

Wipe and rebuild:
```bash
(rm -rf dist/*; npm run postinstall; npm run build; npm run generate)
```

The build is output to .output/server/*.  There is a symlink in the front end root directory (vuenuxt) called "dist" that points to this output dir.

Now you can re-run this web template and use your new front-end!
